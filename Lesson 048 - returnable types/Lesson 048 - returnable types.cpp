/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>
#include <string>

namespace ocl
{

// Define can_return to treats all types that are not 'void' as returnable types.
template<typename Type>
constexpr bool can_return = true;

// Specialise can_return for 'void' to identify the type as not returnable.
template<>
constexpr bool can_return<void> = false;

// Define a class that can identify functions and member functions as returnable.
struct Returnable
{
    template<typename FunctionType, typename... ArgsType>
    static constexpr bool CanReturn(FunctionType func, ArgsType... args) noexcept
    {
        // NOTE: func(args...) and (*func)(args...) are equivalent.
        return can_return<decltype(func(args...))>;
    };

    template<typename ClassType, typename FunctionType, typename... ArgsType>
    static constexpr bool CanReturn(ClassType& object, FunctionType func, ArgsType... args) noexcept
    {
        return can_return<decltype((object.*func)(args...))>;
    };
};

} // namespace ocl

// Example class that will be used by the Returnable implementation.
class Number
{
public:
    Number(int number = 0) noexcept
        : m_number(number)
    {
    }

    void Print()
    {
        std::cout << m_number << std::endl;
    }

    bool Print2()
    {
        std::cout << m_number << std::endl;
        return m_number > 100;
    }

    bool Print3(const int number)
    {
        std::cout << number << std::endl;
        return number > 100;
    }

private:
    int m_number;
};

// Example function that will be used by the Returnable implementation.
void PrintNumber(int number)
{
    std::cout << number << std::endl;
}

bool PrintNumber2(int number)
{
    std::cout << number << std::endl;
    return number > 100;
}

void PrintReturnable(std::string const& message, bool value)
{
    std::string bool_str(value ? "is returnable" : "is not returnable");
    std::cout << message << bool_str << std::endl;
}

int main()
{
    // Examples of identifying functions and member functions of classes as
    // having a void or non-void returning type.

    PrintReturnable("void ", ocl::can_return<void>);
    PrintReturnable("int ", ocl::can_return<int>);

    PrintReturnable("PrintNumber ", ocl::Returnable::CanReturn(&PrintNumber, 1));
    PrintReturnable("PrintNumber2 ", ocl::Returnable::CanReturn(&PrintNumber2, 2));

    Number number(1);
    void (Number::*print_number_ptr)() = &Number::Print;
    bool (Number::*print_number_ptr2)() = &Number::Print2;
    bool (Number::*print_number_ptr3)(int) = &Number::Print3;

    PrintReturnable("Number::Print ", ocl::Returnable::CanReturn(number, print_number_ptr));
    PrintReturnable("Number::Print2 ", ocl::Returnable::CanReturn(number, print_number_ptr2));
    PrintReturnable("Number::Print3 ", ocl::Returnable::CanReturn(number, print_number_ptr3, 3));

    return 0;
}
