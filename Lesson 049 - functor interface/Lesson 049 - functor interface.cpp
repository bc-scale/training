/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>
#include <string>

// Define an interface that can be used to execute a function or member function of a class.
template<typename ReturnType, typename... ArgsType>
class IFunctor
{
public:
    typedef ReturnType return_type;

public:
    virtual return_type Execute(ArgsType... args) = 0;

    // Implement the functor operator.
    return_type operator()(ArgsType... args)
    {
        return Execute(args...);
    }
};

// Implement a class that wraps a function and can be
// executed one or more times with different arguments.
template<typename FuncionType, typename ReturnType, typename... ArgsType>
class Function : public IFunctor<ReturnType, ArgsType...>
{
public:
    Function(FuncionType func)
        : m_func(func)
    {
    }

    ReturnType Execute(ArgsType... args) override
    {
        return (*m_func)(args...);
    }

private:
    FuncionType m_func;
};

// Specialise for void return, ensuring the Execute function does not return.
template<typename FuncionType, typename... ArgsType>
class Function< FuncionType, void, ArgsType...> : public IFunctor<void, ArgsType...>
{
public:
    Function(FuncionType func)
        : m_func(func)
    {
    }

    void Execute(ArgsType... args) override
    {
        (*m_func)(args...);
    }

private:
    FuncionType m_func;
};

// Implement a class that wraps a member function of a class and can be
// executed one or more times with different arguments.
template<typename ClassType, typename ReturnType, typename... ArgsType>
class MemberFunction : public IFunctor<ReturnType, ArgsType...>
{
public:
    MemberFunction(ClassType& object, ReturnType (ClassType::* func)(ArgsType...))
        : m_object(object)
        , m_func(func)
    {
    }

    ReturnType Execute(ArgsType... args) override
    {
        return (m_object.*m_func)(args...);
    }

private:
    ClassType& m_object;
    ReturnType (ClassType::*m_func)(ArgsType...);
};

// Specialise for void return, ensuring the Execute function does not return.
template<typename ClassType, typename... ArgsType>
class MemberFunction<ClassType, void, ArgsType...> : public IFunctor<void, ArgsType...>
{
public:
    MemberFunction(ClassType& object, void (ClassType::* func)(ArgsType...))
        : m_object(object)
        , m_func(func)
    {
    }

    void Execute(ArgsType... args) override
    {
        (m_object.*m_func)(args...);
    }

private:
    ClassType& m_object;
    void (ClassType::*m_func)(ArgsType...);
};


// Example class that will be used by the MemberFunction implementation of the IFunctor interface.
class Number
{
public:
    Number(int number = 0) noexcept
        : m_number(number)
    {
    }

    void SetNumber(int number) noexcept
    {
        m_number = number;
    }

    void Print()
    {
        std::cout << m_number << std::endl;
    }

    bool Print2()
    {
        std::cout << m_number << std::endl;
        return m_number > 100;
    }

    bool Print3(int number)
    {
        std::cout << number << std::endl;
        return number > 100;
    }

private:
    int m_number;
};

// Example function that will be used by the MemberFunction implementation of the IFunctor interface.
void PrintNumber(int number)
{
    std::cout << number << std::endl;
}

bool PrintNumber2(int number)
{
    std::cout << number << std::endl;
    return number > 100;
}

int main()
{
    Function<void(*)(int), void, int> func(&PrintNumber);
    Function<bool(*)(int), bool, int> func2(&PrintNumber2);

    Number number;
    MemberFunction<Number, void> member_func(number, &Number::Print);
    MemberFunction<Number, bool> member_func2(number, &Number::Print2);
    MemberFunction<Number, bool, int> member_func3(number, &Number::Print3);

    // Execute function that takes int parameter and has no return.
    func.Execute(99);

    // Execute function that takes int parameter and has bool return.
    bool is_large = func2.Execute(101);
    std::cout << (is_large ? "is large" : "is not large") << std::endl;

    // Execute member function that takes no parameter and has no return.
    number.SetNumber(98);
    member_func.Execute();

    // Execute member function that takes no parameter and has bool return.
    number.SetNumber(102);
    is_large = member_func2.Execute();
    std::cout << (is_large ? "is large" : "is not large") << std::endl;

    // Execute member function that takes int parameter and has no return.
    is_large = member_func3.Execute(3);
    std::cout << (is_large ? "is large" : "is not large") << std::endl;

    // Examples of using operator() instead of Execute.
    number.SetNumber(97);
    member_func();
    number.SetNumber(103);
    is_large = member_func2();
    std::cout << (is_large ? "is large" : "is not large") << std::endl;

    return 0;
}
