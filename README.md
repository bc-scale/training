# training

![](header_image.jpg)

C++ training material covering: data types, pointers, references, operators and operator precedence, conditions, loops, classes, encapsulation, inheritance, polymorphism, templates, standard library, additional language features.

Downloading the git tools can be done from here:
https://git-scm.com/

The source code can be obtained by using the following command at a command prompt:  
git clone https://gitlab.com/cppocl/training

(remember to run this command from the folder where you wish to put the files and folders)

The training is written for Visual Studio 2015 and 2019, but all examples should build on Linux with GCC.

The training is designed to start for a beginner and build up to an experienced level, so keep watching...
