/*
Copyright 2021 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>

bool PrintNumber(int number)
{
    std::cout << number << std::endl;
    return number > 100;
}

class Number
{
public:
    static bool Print(int number)
    {
        std::cout << number << std::endl;
        return number > 100;
    }
};

int main()
{
    // Define a pointer to function variable called print_number_ptr and assign it.
    bool (*print_number_ptr)(int) = &PrintNumber;

    // Use the pointer to function variable to call print_number function.
    bool is_large = (*print_number_ptr)(99);
    std::string message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    is_large = (*print_number_ptr)(101);
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    // Perform the same pointer to function tasks with the static member function Number::Print.
    print_number_ptr = &Number::Print;
    is_large = (*print_number_ptr)(98);
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    is_large = (*print_number_ptr)(102);
    message = is_large ? "large number" : "small number";
    std::cout << message << std::endl;

    return 0;
}
